﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace MLSincronizador.Clases
{
    [Serializable]
    public class Variantes
    {
        public List<Variante> items { get; set; } 
      
    }
    [Serializable]
    public class Variante
    {
        public string id { get; set; }
        public List<Combinaciones> attribute_combinations { get; set; }
    }

    public class Combinaciones
    {
        public string id { get; set; }

    }
}
