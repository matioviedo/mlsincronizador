﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MercadoLibre.SDK;

namespace MLSincronizador.Clases.DataAccess
{
    public class MLManager
    {
        //public string ActualizarStockYPrecio()
        //{
        //    string strRetorno = string.Empty;
        //    DataTable oDT = new DataTable();
        //    string AccessToken = string.Empty;
        //    string RefreshToken = string.Empty;
        //    Meli m;
        //    DBManager oDB = new DBManager();
        //    DataTable oDTSubidos = new DataTable();
        //    DataTable oDTArticulos = new DataTable();
        //    try
        //    {
                
        //        oDT = oDB.ObtenerToken();

        //        if (oDT.Rows.Count > 0)
        //        {
        //            AccessToken = oDT.Rows[0]["token"].ToString();
        //            RefreshToken = oDT.Rows[0]["refresh_token"].ToString();

        //            if (Convert.ToDateTime(oDT.Rows[0]["expires_in"]) <= DateTime.Now)
        //            {
        //                m = new Meli(Convert.ToInt64(ConfigurationManager.AppSettings["APP_ID"]), ConfigurationManager.AppSettings["APP_Secret"].ToString(), AccessToken, RefreshToken);

        //                //Se actualiza el token en BD para nuevos ingresos
        //                oDB.ActualizarToken(m.AccessToken, m.RefreshToken, 21600);

        //                AccessToken = m.AccessToken;
        //                RefreshToken = m.RefreshToken;
        //            }
        //            else
        //            {
        //                m = new Meli(Convert.ToInt64(ConfigurationManager.AppSettings["APP_ID"]), ConfigurationManager.AppSettings["APP_Secret"].ToString(), AccessToken);
        //            }

        //            oDTSubidos = oDB.SubidosSuiza_GetAll();
        //            oDTArticulos = oDB.ArticulosSuiza_GetAll();

        //            foreach (DataRow oSubido in oDTSubidos.Rows)
        //            {
        //                DataRow[] oRowArticulo = oDTArticulos.Select("IdArticulo=" + oSubido["IdArticulo"]);

        //                if (oRowArticulo.Length > 0)
        //                {
        //                    bool Cambio = false;
        //                    //Por cada Subido se verifica si el articulo varió en Precio o Stock
        //                    if (oSubido["Stock"].ToString() != oRowArticulo[0]["stockactual"].ToString())
        //                    {
        //                        Cambio = true;
        //                    }
        //                    if (Convert.ToDecimal(oSubido["Precio"]) != GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])))
        //                    {
        //                        Cambio = true;
        //                    }
        //                    if (Cambio)
        //                    {
        //                        #region Parametros Conexion
        //                        var p = new RestSharp.Parameter();
        //                        p.Name = "access_token";
        //                        p.Value = AccessToken;

        //                        var ps = new List<RestSharp.Parameter>();
        //                        ps.Add(p);
        //                        #endregion

        //                        //Verificar si tiene variantes
        //                        //var variantes = m.Get("/items/" + oSubido["IdMercadoLibre"].ToString() + "/variations/", ps);
        //                        //var respuesta = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Variantes>>(variantes.Content);

        //                        var r = m.Put("/items/" + oSubido["IdMercadoLibre"].ToString(), ps,
        //                                        new
        //                                        {
        //                                            price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
        //                                            available_quantity = oRowArticulo[0]["stockactual"].ToString()
        //                                        });

        //                        if (r.StatusCode == System.Net.HttpStatusCode.OK)
        //                        {
        //                            //Se actualiza el registro en Subido
        //                            oDB.SubidosSuiza_Update(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), oSubido["IdMercadoLibre"].ToString(), Convert.ToInt64(oRowArticulo[0]["stockactual"]), GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])));
        //                        }
        //                        else
        //                        {
        //                            oDB.Error_Insert(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), r.Content, r.Request.Parameters.LastOrDefault().ToString());
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    //Se deberia pausar o eliminar la publicación
        //                    //Colocar FechaBaja al registro de Subido
        //                }

        //            }

        //            //var response = m.Get("/items/MLA723143596");

        //            //MessageBox.Show(response.Content);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return strRetorno;
        //}

        //private decimal GetPrecioConIVA(decimal pPrecio, decimal pIVA)
        //{
        //    decimal Retorno = 0;

        //    Retorno = pPrecio + (pPrecio * pIVA) / 100;

        //    return Retorno;
        //}
    }
}
