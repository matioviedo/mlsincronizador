﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;

namespace MLSincronizador.Clases.DataAccess
{
    public class DBManager
    {
        SqlConnection oCon;
        SqlCommand oCmd;

        #region "SUIZA"

        public void ArticulosMaestro_Insert()
        {
            try
            {
                DataSet oDSSuiza = new DataSet();
                DataSet oDSArmeria = new DataSet();

                try
                {
                    oDSSuiza.ReadXml("http://www.suizaoutdoor.com.ar/Carrito/Datos/xmlArticulos.xml");
                }
                catch (Exception)
                {
                    //Espera 10 minutos para volver a leer el archivo
                    Thread.Sleep(600000);
                    oDSSuiza.ReadXml("http://www.suizaoutdoor.com.ar/Carrito/Datos/xmlArticulos.xml");
                }

                DataTable dtArticulosSuiza = oDSSuiza.Tables["Articulos"];

                try
                {
                    oDSArmeria.ReadXml("http://www.armeriasuiza.com.ar/Datos/xmlArticulos.xml");
                }
                catch (Exception)
                {
                    //Espera 10 minutos para volver a leer el archivo
                    Thread.Sleep(600000);
                    oDSArmeria.ReadXml("http://www.armeriasuiza.com.ar/Datos/xmlArticulos.xml");
                }

                DataTable dtArticulosArmeria = oDSArmeria.Tables["Articulos"];

                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Borra los arcticulos
                oCmd.CommandText = "Maestro_Articulo_t";
                oCmd.ExecuteNonQuery();

                oCmd.Parameters.Clear();

                //Inserta los articulos desde el XML
                oCmd.CommandText = "Maestro_Articulo_i";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Codigo", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Nombre", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Descripcion", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Marca", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdRubro", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Rubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@SubRubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@SubSubRubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Categoria", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Precio", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Iva", SqlDbType.Decimal);
                oCmd.Parameters.Add("@CodEAN", SqlDbType.VarChar);

                //Suiza
                foreach (DataRow oArt in dtArticulosSuiza.Rows)
                {
                    oCmd.Parameters["@IdArticulo"].Value = oArt["IdArticulo"];
                    oCmd.Parameters["@Codigo"].Value = oArt["Codigo"];
                    oCmd.Parameters["@Nombre"].Value = oArt["Descripcion"].ToString();
                    oCmd.Parameters["@Descripcion"].Value = oArt["Detalles"].ToString();
                    oCmd.Parameters["@Marca"].Value = oArt["Marca"].ToString();
                    oCmd.Parameters["@IdRubro"].Value = oArt["IdRubro"];
                    oCmd.Parameters["@Rubro"].Value = oArt["Rubro"].ToString();
                    oCmd.Parameters["@SubRubro"].Value = oArt["SubRubro"].ToString();
                    oCmd.Parameters["@SubSubRubro"].Value = oArt["SubSubRubro"].ToString();
                    oCmd.Parameters["@Categoria"].Value = oArt["Categoria"].ToString();
                    oCmd.Parameters["@Precio"].Value = oArt["PrecioContado"];
                    oCmd.Parameters["@Iva"].Value = oArt["Iva"];
                    oCmd.Parameters["@CodEAN"].Value = oArt["CodEAN"];

                    oCmd.ExecuteNonQuery();
                }

                //Armeria
                foreach (DataRow oArt in dtArticulosArmeria.Rows)
                {
                    oCmd.Parameters["@IdArticulo"].Value = oArt["IdArticulo"];
                    oCmd.Parameters["@Codigo"].Value = oArt["Codigo"];
                    oCmd.Parameters["@Nombre"].Value = oArt["Descripcion"].ToString();
                    oCmd.Parameters["@Descripcion"].Value = oArt["Detalles"].ToString();
                    oCmd.Parameters["@Marca"].Value = oArt["Marca"].ToString();
                    oCmd.Parameters["@IdRubro"].Value = oArt["IdRubro"];
                    oCmd.Parameters["@Rubro"].Value = oArt["Rubro"].ToString();
                    oCmd.Parameters["@SubRubro"].Value = oArt["SubRubro"].ToString();
                    oCmd.Parameters["@SubSubRubro"].Value = oArt["SubSubRubro"].ToString();
                    oCmd.Parameters["@Categoria"].Value = oArt["Categoria"].ToString();
                    oCmd.Parameters["@Precio"].Value = oArt["PrecioContado"];
                    oCmd.Parameters["@Iva"].Value = oArt["Iva"];
                    oCmd.Parameters["@CodEAN"].Value = oArt["CodEAN"];

                    oCmd.ExecuteNonQuery();
                }

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ArticulosSuiza_Insert()
        {
            try
            {
                DataSet oDS = new DataSet();
                try
                {
                    oDS.ReadXml("http://www.suizaoutdoor.com.ar/Carrito/Datos/xmlArticulosTalle.xml");
                }
                catch (Exception)
                {
                    //Espera 10 minutos para volver a leer el archivo
                    Thread.Sleep(600000);
                    oDS.ReadXml("http://www.suizaoutdoor.com.ar/Carrito/Datos/xmlArticulosTalle.xml");
                }

                DataTable dtArticulos = oDS.Tables["Articulos"];

                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Borra los arcticulos
                oCmd.CommandText = "Articulo_t";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = "SUIZA";
                oCmd.ExecuteNonQuery();

                oCmd.Parameters.Clear();

                //Inserta los articulos desde el XML
                oCmd.CommandText = "Articulo_i";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Nombre", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Descripcion", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Marca", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdRubro", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Rubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@SubRubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@SubSubRubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Categoria", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Precio", SqlDbType.Decimal);
                oCmd.Parameters.Add("@PrecioSinIva", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Iva", SqlDbType.Decimal);
                oCmd.Parameters.Add("@CodEAN", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Codigo", SqlDbType.VarChar);
                oCmd.Parameters.Add("@CodigoProveedor", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Link", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Oferta", SqlDbType.Bit);
                oCmd.Parameters.Add("@Destacado", SqlDbType.Bit);
                oCmd.Parameters.Add("@Outlet", SqlDbType.Bit);
                oCmd.Parameters.Add("@MasVendido", SqlDbType.Bit);
                oCmd.Parameters.Add("@Alto", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Ancho", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Fondo", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Peso", SqlDbType.Decimal);

                foreach (DataRow oArt in dtArticulos.Rows)
                {
                    oCmd.Parameters["@IdArticulo"].Value = oArt["IdArticulo"];
                    oCmd.Parameters["@Empresa"].Value = "SUIZA";
                    oCmd.Parameters["@Nombre"].Value = oArt["Descripcion"].ToString();
                    oCmd.Parameters["@Descripcion"].Value = oArt["Detalles"].ToString();
                    oCmd.Parameters["@Marca"].Value = oArt["Marca"].ToString();
                    oCmd.Parameters["@IdRubro"].Value = oArt["IdRubro"];
                    oCmd.Parameters["@Rubro"].Value = oArt["Rubro"].ToString();
                    oCmd.Parameters["@SubRubro"].Value = oArt["SubRubro"].ToString();
                    oCmd.Parameters["@SubSubRubro"].Value = oArt["SubSubRubro"].ToString();
                    oCmd.Parameters["@Categoria"].Value = oArt["Categoria"].ToString();
                    oCmd.Parameters["@Precio"].Value = oArt["PrecioContado"];
                    oCmd.Parameters["@PrecioSinIva"].Value = oArt["PrecioSinIva"];
                    oCmd.Parameters["@Iva"].Value = oArt["Iva"];
                    oCmd.Parameters["@CodEAN"].Value = oArt["CodEAN"];
                    oCmd.Parameters["@Codigo"].Value = oArt["Codigo"];
                    oCmd.Parameters["@CodigoProveedor"].Value = oArt["CodigoProveedor"];
                    oCmd.Parameters["@Link"].Value = oArt["Link"];
                    oCmd.Parameters["@Oferta"].Value = oArt["Oferta"];
                    oCmd.Parameters["@Destacado"].Value = oArt["Destacado"];
                    oCmd.Parameters["@Outlet"].Value = oArt["Outlet"];
                    oCmd.Parameters["@MasVendido"].Value = oArt["MasVendido"];
                    oCmd.Parameters["@Alto"].Value = oArt["Alto"];
                    oCmd.Parameters["@Ancho"].Value = oArt["Ancho"];
                    oCmd.Parameters["@Fondo"].Value = oArt["Fondo"];
                    oCmd.Parameters["@Peso"].Value = oArt["Peso"];

                    oCmd.ExecuteNonQuery();
                }

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TallesSuiza_Insert()
        {
            try
            {
                DataSet oDS = new DataSet();
                try
                {
                    oDS.ReadXml("http://www.suizaoutdoor.com.ar/Carrito/Datos/xmlArticulosTalle.xml");
                }
                catch (Exception)
                {
                    //Espera 10 minutos para volver a leer el archivo
                    Thread.Sleep(600000);
                    oDS.ReadXml("http://www.suizaoutdoor.com.ar/Carrito/Datos/xmlArticulosTalle.xml");
                }
                DataTable dtTalles = oDS.Tables["Talles"];

                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Borra los talles
                oCmd.CommandText = "Talle_t";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = "SUIZA";
                oCmd.ExecuteNonQuery();
                oCmd.Parameters.Clear();

                //Inserta los talles desde el XML
                oCmd.CommandText = "Talle_i";

                oCmd.Parameters.Add("@IdPadre", SqlDbType.BigInt);
                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Talle", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdTalleML", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdColorP", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdColorS", SqlDbType.VarChar);
                oCmd.Parameters.Add("@CodEAN", SqlDbType.VarChar);

                foreach (DataRow oTalle in dtTalles.Rows)
                {
                    oCmd.Parameters["@IdPadre"].Value = oTalle["IdPadre"];
                    oCmd.Parameters["@IdArticulo"].Value = oTalle["IdArticulo"];
                    oCmd.Parameters["@Empresa"].Value = "SUIZA";
                    oCmd.Parameters["@Talle"].Value = oTalle["Talle"].ToString();
                    oCmd.Parameters["@IdTalleML"].Value = oTalle["IdTalleML"].ToString();
                    oCmd.Parameters["@IdColorP"].Value = oTalle["IdColorP"].ToString();
                    oCmd.Parameters["@IdColorS"].Value = oTalle["IdColorS"].ToString();
                    oCmd.Parameters["@CodEAN"].Value = oTalle["CodEAN"].ToString();

                    oCmd.ExecuteNonQuery();
                }

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void StockSuiza_Insert()
        {
            try
            {
                DataSet oDS = new DataSet();
                try
                {
                    oDS.ReadXml("http://www.suizaoutdoor.com.ar/Carrito/Datos/xmlStock.xml");
                }
                catch (Exception)
                {
                    //Espera 10 minutos para volver a leer el archivo
                    Thread.Sleep(600000);
                    oDS.ReadXml("http://www.suizaoutdoor.com.ar/Carrito/Datos/xmlStock.xml");
                }

                DataTable dtStock = oDS.Tables["Stock"];

                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Borra los talles
                oCmd.CommandText = "Stock_t";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = "SUIZA";
                oCmd.ExecuteNonQuery();
                oCmd.Parameters.Clear();

                //Inserta los talles desde el XML
                oCmd.CommandText = "Stock_i";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdSucursal", SqlDbType.Int);
                oCmd.Parameters.Add("@NombreSucursal", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Stock", SqlDbType.Int);

                foreach (DataRow oStock in dtStock.Rows)
                {
                    oCmd.Parameters["@IdArticulo"].Value = oStock["IdArticulo"];
                    oCmd.Parameters["@Empresa"].Value = "SUIZA";
                    oCmd.Parameters["@IdSucursal"].Value = oStock["IdSucursal"];
                    oCmd.Parameters["@NombreSucursal"].Value = oStock["Descripcion"].ToString();
                    oCmd.Parameters["@Stock"].Value = oStock["StockActual"];

                    oCmd.ExecuteNonQuery();
                }

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Subidos_GetAll(string Empresa)
        {
            DataTable oDT = new DataTable();
            oCon = new SqlConnection();
            oCmd = new SqlCommand();
            try
            {
                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Obtiene los subidos
                oCmd.CommandText = "Subido_g";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = Empresa;

                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();
                oCon.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDT;
        }

        public DataTable Articulos_GetAll(string Empresa)
        {
            DataTable oDT = new DataTable();
            oCon = new SqlConnection();
            oCmd = new SqlCommand();
            try
            {
                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Obtiene los subidos
                oCmd.CommandText = "Articulo_g";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = Empresa;

                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();
                oCon.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDT;
        }

        public DataTable ArticulosNuevos_GetAll(string Empresa)
        {
            DataTable oDT = new DataTable();
            oCon = new SqlConnection();
            oCmd = new SqlCommand();
            try
            {
                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Obtiene los subidos
                oCmd.CommandText = "Articulo_g_nuevos";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = Empresa;

                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();
                oCon.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDT;
        }

        public DataTable Talles_GetAll(string Empresa)
        {
            DataTable oDT = new DataTable();
            oCon = new SqlConnection();
            oCmd = new SqlCommand();
            try
            {
                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Obtiene los subidos
                oCmd.CommandText = "Talle_g";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = Empresa;

                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();
                oCon.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDT;
        }

        public DataTable Stock_GetAll(string Empresa)
        {
            DataTable oDT = new DataTable();
            oCon = new SqlConnection();
            oCmd = new SqlCommand();
            try
            {
                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Obtiene los subidos
                oCmd.CommandText = "Stock_g";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = Empresa;

                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();
                oCon.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDT;
        }

        public DataTable Stock_GetBy(Int64 IdArticulo)
        {
            DataTable oDT = new DataTable();
            oCon = new SqlConnection();
            oCmd = new SqlCommand();
            try
            {
                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Obtiene el total de stock por talle de un articulo
                oCmd.CommandText = "Stock_g_by";
                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters["@IdArticulo"].Value = IdArticulo;

                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();
                oCon.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDT;

        }

        public DataTable MapeoCategoria_GetAll()
        {
            DataTable oDT = new DataTable();
            oCon = new SqlConnection();
            oCmd = new SqlCommand();
            try
            {
                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Obtiene lo mapeado
                oCmd.CommandText = "MapeoCategoria_g";

                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();
                oCon.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDT;
        }

        public DataTable ParaFinalizar_GetAll()
        {
            DataTable oDT = new DataTable();
            oCon = new SqlConnection();
            oCmd = new SqlCommand();
            try
            {
                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.Text;
                //Obtiene los subidos
                oCmd.CommandText = "select * from ParaFinalizar";

                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();
                oCon.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDT;
        }

        public void Subidos_Update(Int64 IdArticulo, String Empresa, String IdMercadoLibre, Int64 Stock, decimal Precio)
        {
            try
            {
                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;

                //Inserta los talles desde el XML
                oCmd.CommandText = "Subido_u";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdMercadoLibre", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Stock", SqlDbType.Int);
                oCmd.Parameters.Add("@Precio", SqlDbType.Decimal);

                oCmd.Parameters["@IdArticulo"].Value = IdArticulo;
                oCmd.Parameters["@Empresa"].Value = Empresa;
                oCmd.Parameters["@IdMercadoLibre"].Value = IdMercadoLibre;
                oCmd.Parameters["@Precio"].Value = Precio;
                oCmd.Parameters["@Stock"].Value = Stock;

                oCmd.ExecuteNonQuery();

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Nuevos_Insert(Int64 IdArticulo, String Empresa, String IdMercadoLibre, Int64 Stock, decimal Precio)
        {
            try
            {
                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;

                oCmd.CommandText = "Subido_i";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdMercadoLibre", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Stock", SqlDbType.Int);
                oCmd.Parameters.Add("@Precio", SqlDbType.Decimal);

                oCmd.Parameters["@IdArticulo"].Value = IdArticulo;
                oCmd.Parameters["@Empresa"].Value = Empresa;
                oCmd.Parameters["@IdMercadoLibre"].Value = IdMercadoLibre;
                oCmd.Parameters["@Precio"].Value = Precio;
                oCmd.Parameters["@Stock"].Value = Stock;

                oCmd.ExecuteNonQuery();

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Armeria"

        public void ArticulosArmeria_Insert()
        {
            try
            {
                DataSet oDS = new DataSet();
                try
                {
                    oDS.ReadXml("http://www.armeriasuiza.com.ar/Datos/xmlArticulosTalle.xml");
                }
                catch (Exception)
                {
                    //Espera 10 minutos para volver a leer el archivo
                    Thread.Sleep(600000);
                    oDS.ReadXml("http://www.armeriasuiza.com.ar/Datos/xmlArticulosTalle.xml");
                }
                DataTable dtArticulos = oDS.Tables["Articulos"];

                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Borra los arcticulos
                oCmd.CommandText = "Articulo_t";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = "ARMERIA";
                oCmd.ExecuteNonQuery();

                oCmd.Parameters.Clear();

                //Inserta los articulos desde el XML
                oCmd.CommandText = "Articulo_i";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Nombre", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Descripcion", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Marca", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdRubro", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Rubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@SubRubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@SubSubRubro", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Categoria", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Precio", SqlDbType.Decimal);
                oCmd.Parameters.Add("@PrecioSinIva", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Iva", SqlDbType.Decimal);
                oCmd.Parameters.Add("@CodEAN", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Codigo", SqlDbType.VarChar);
                oCmd.Parameters.Add("@CodigoProveedor", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Link", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Oferta", SqlDbType.Bit);
                oCmd.Parameters.Add("@Destacado", SqlDbType.Bit);
                oCmd.Parameters.Add("@Outlet", SqlDbType.Bit);
                oCmd.Parameters.Add("@MasVendido", SqlDbType.Bit);
                oCmd.Parameters.Add("@Alto", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Ancho", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Fondo", SqlDbType.Decimal);
                oCmd.Parameters.Add("@Peso", SqlDbType.Decimal);

                foreach (DataRow oArt in dtArticulos.Rows)
                {
                    oCmd.Parameters["@IdArticulo"].Value = oArt["IdArticulo"];
                    oCmd.Parameters["@Empresa"].Value = "ARMERIA";
                    oCmd.Parameters["@Nombre"].Value = oArt["Descripcion"].ToString();
                    oCmd.Parameters["@Descripcion"].Value = oArt["Detalles"].ToString();
                    oCmd.Parameters["@Marca"].Value = oArt["Marca"].ToString();
                    oCmd.Parameters["@IdRubro"].Value = oArt["IdRubro"];
                    oCmd.Parameters["@Rubro"].Value = oArt["Rubro"].ToString();
                    oCmd.Parameters["@SubRubro"].Value = oArt["SubRubro"].ToString();
                    oCmd.Parameters["@SubSubRubro"].Value = oArt["SubSubRubro"].ToString();
                    oCmd.Parameters["@Categoria"].Value = oArt["Categoria"].ToString();
                    oCmd.Parameters["@Precio"].Value = oArt["PrecioContado"];
                    oCmd.Parameters["@PrecioSinIva"].Value = oArt["PrecioSinIva"];
                    oCmd.Parameters["@Iva"].Value = oArt["Iva"];
                    oCmd.Parameters["@CodEAN"].Value = oArt["CodEAN"].ToString();
                    oCmd.Parameters["@Codigo"].Value = oArt["Codigo"];
                    oCmd.Parameters["@CodigoProveedor"].Value = oArt["CodigoProveedor"];
                    oCmd.Parameters["@Link"].Value = oArt["Link"];
                    oCmd.Parameters["@Oferta"].Value = oArt["Oferta"];
                    oCmd.Parameters["@Destacado"].Value = oArt["Destacado"];
                    oCmd.Parameters["@Outlet"].Value = oArt["Outlet"];
                    oCmd.Parameters["@MasVendido"].Value = oArt["MasVendido"];
                    oCmd.Parameters["@Alto"].Value = oArt["Alto"];
                    oCmd.Parameters["@Ancho"].Value = oArt["Ancho"];
                    oCmd.Parameters["@Fondo"].Value = oArt["Fondo"];
                    oCmd.Parameters["@Peso"].Value = oArt["Peso"];

                    oCmd.ExecuteNonQuery();
                }

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TallesArmeria_Insert()
        {
            try
            {
                DataSet oDS = new DataSet();
                try
                {
                    oDS.ReadXml("http://www.armeriasuiza.com.ar/Datos/xmlArticulosTalle.xml");
                }
                catch (Exception)
                {
                    //Espera 10 minutos para volver a leer el archivo
                    Thread.Sleep(600000);
                    oDS.ReadXml("http://www.armeriasuiza.com.ar/Datos/xmlArticulosTalle.xml");
                }
                DataTable dtTalles = oDS.Tables["Talles"];

                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Borra los talles
                oCmd.CommandText = "Talle_t";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = "ARMERIA";
                oCmd.ExecuteNonQuery();
                oCmd.Parameters.Clear();

                //Inserta los talles desde el XML
                oCmd.CommandText = "Talle_i";

                oCmd.Parameters.Add("@IdPadre", SqlDbType.BigInt);
                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Talle", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdTalleML", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdColorP", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdColorS", SqlDbType.VarChar);
                oCmd.Parameters.Add("@CodEAN", SqlDbType.VarChar);


                foreach (DataRow oTalle in dtTalles.Rows)
                {
                    oCmd.Parameters["@IdPadre"].Value = oTalle["IdPadre"];
                    oCmd.Parameters["@IdArticulo"].Value = oTalle["IdArticulo"];
                    oCmd.Parameters["@Empresa"].Value = "ARMERIA";
                    oCmd.Parameters["@Talle"].Value = oTalle["Talle"].ToString();
                    oCmd.Parameters["@IdTalleML"].Value = oTalle["IdTalleML"].ToString();
                    oCmd.Parameters["@IdColorP"].Value = oTalle["IdColorP"].ToString();
                    oCmd.Parameters["@IdColorS"].Value = oTalle["IdColorS"].ToString();
                    oCmd.Parameters["@CodEAN"].Value = "";

                    oCmd.ExecuteNonQuery();
                }

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void StockArmeria_Insert()
        {
            try
            {
                DataSet oDS = new DataSet();
                try
                {
                    oDS.ReadXml("http://www.armeriasuiza.com.ar/Datos/xmlStock.xml");
                }
                catch (Exception)
                {
                    //Espera 10 minutos para volver a leer el archivo
                    Thread.Sleep(600000);
                    oDS.ReadXml("http://www.armeriasuiza.com.ar/Datos/xmlStock.xml");
                }
                DataTable dtStock = oDS.Tables["Stock"];

                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;
                //Borra los talles
                oCmd.CommandText = "Stock_t";
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters["@Empresa"].Value = "ARMERIA";
                oCmd.ExecuteNonQuery();
                oCmd.Parameters.Clear();

                //Inserta los talles desde el XML
                oCmd.CommandText = "Stock_i";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@IdSucursal", SqlDbType.Int);
                oCmd.Parameters.Add("@NombreSucursal", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Stock", SqlDbType.Int);

                foreach (DataRow oStock in dtStock.Rows)
                {
                    oCmd.Parameters["@IdArticulo"].Value = oStock["IdArticulo"];
                    oCmd.Parameters["@Empresa"].Value = "ARMERIA";
                    oCmd.Parameters["@IdSucursal"].Value = oStock["IdSucursal"];
                    oCmd.Parameters["@NombreSucursal"].Value = oStock["Descripcion"].ToString();
                    oCmd.Parameters["@Stock"].Value = oStock["StockActual"];

                    oCmd.ExecuteNonQuery();
                }

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public void Error_Truncate()
        {
            try
            {
                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;

                //Borra todos los errores antes de un nuevo procesamiento
                oCmd.CommandText = "Error_t";

                oCmd.ExecuteNonQuery();

                oCmd.CommandText = "ErrorDatos_t";

                oCmd.ExecuteNonQuery();

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Error_Insert(Int64 IdArticulo, String Empresa, String Response, String JSon)
        {
            try
            {
                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;

                //Inserta los talles desde el XML
                oCmd.CommandText = "Error_i";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Empresa", SqlDbType.VarChar);
                oCmd.Parameters.Add("@Response", SqlDbType.VarChar);
                oCmd.Parameters.Add("@JSon", SqlDbType.VarChar);

                oCmd.Parameters["@IdArticulo"].Value = IdArticulo;
                oCmd.Parameters["@Empresa"].Value = Empresa;
                oCmd.Parameters["@Response"].Value = Response;
                oCmd.Parameters["@JSon"].Value = JSon;

                oCmd.ExecuteNonQuery();

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ErrorDatos_Insert(Int64 IdArticulo, String Mensaje)
        {
            try
            {
                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandType = CommandType.StoredProcedure;

                //Inserta los talles desde el XML
                oCmd.CommandText = "ErrorDatos_i";

                oCmd.Parameters.Add("@IdArticulo", SqlDbType.BigInt);
                oCmd.Parameters.Add("@Mensaje", SqlDbType.VarChar);

                oCmd.Parameters["@IdArticulo"].Value = IdArticulo;
                oCmd.Parameters["@Mensaje"].Value = Mensaje;

                oCmd.ExecuteNonQuery();

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerToken()
        {
            SqlConnection oCon;
            SqlCommand oCmd;
            DataTable oDT;
            try
            {
                oCon = new SqlConnection();
                oCmd = new SqlCommand();
                oDT = new DataTable();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandText = "select * from Autenticacion";
                oDT.Load(oCmd.ExecuteReader());

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();

            }
            catch (Exception)
            {
                throw;
            }

            return oDT;
        }

        public void ActualizarToken(string token, string refresh_token, double expires_in)
        {
            SqlConnection oCon;
            SqlCommand oCmd;
            try
            {
                oCon = new SqlConnection();
                oCmd = new SqlCommand();

                oCon.ConnectionString = ConfigurationManager.ConnectionStrings["MLSincro"].ToString();
                oCon.Open();

                oCmd.Connection = oCon;
                oCmd.CommandText = "Autenticacion_u";
                oCmd.CommandType = CommandType.StoredProcedure;

                oCmd.Parameters.Add("@token", SqlDbType.VarChar);
                oCmd.Parameters.Add("@refresh_token", SqlDbType.VarChar);
                oCmd.Parameters.Add("@expires_in", SqlDbType.DateTime);

                oCmd.Parameters["@token"].Value = token;
                oCmd.Parameters["@refresh_token"].Value = refresh_token;
                oCmd.Parameters["@expires_in"].Value = DateTime.Now.AddSeconds(expires_in).ToString("yyyy-MM-dd HH:mm:ss");

                oCmd.ExecuteNonQuery();

                oCmd.Dispose();
                oCon.Close();

                oCon.Dispose();

            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}
