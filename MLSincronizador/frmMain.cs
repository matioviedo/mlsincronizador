﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MLSincronizador.Clases;
using MLSincronizador.Clases.DataAccess;
using System.Data.SqlClient;
using System.Configuration;
using MercadoLibre.SDK;
using System.Net.Mail;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace MLSincronizador
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            DateTime dtInicio = DateTime.Now;
            try
            {
                Application.DoEvents();

                DBManager oDB = new DBManager();

                //Maestro de Articulos de Sinergia
                oDB.ArticulosMaestro_Insert();

                //Articulos, Talles y Stock de Outdoor
                oDB.ArticulosSuiza_Insert();
                oDB.TallesSuiza_Insert();
                oDB.StockSuiza_Insert();

                //Articulos, Talles y Stock de Armeria
                oDB.ArticulosArmeria_Insert();
                oDB.TallesArmeria_Insert();
                oDB.StockArmeria_Insert();

                //Rutina de Sincronización de Mercado Libre
                ActualizarStockYPrecio();

                //Envía Email de Notificación OK
                EnviarMail(dtInicio, DateTime.Now, false);

            }
            catch (Exception ex)
            {
                //Envia Email de Notificación de FALLO
                EnviarMail(dtInicio, DateTime.Now, true, ex.Message);
            }
            this.Close();
        }

        private void ActualizarStockYPrecio()
        {
            string strRetorno = string.Empty;
            DataTable oDT = new DataTable();
            string AccessToken = string.Empty;
            string RefreshToken = string.Empty;
            Meli m;
            DBManager oDB = new DBManager();
            System.Globalization.CultureInfo oCi = System.Globalization.CultureInfo.InvariantCulture;
            bool blnSincronizaDescripciones = false;
            try
            {
                //Se obtiene el Token desde Base de Datos
                oDT = oDB.ObtenerToken();

                if (oDT.Rows.Count > 0)
                {
                    AccessToken = oDT.Rows[0]["token"].ToString();
                    RefreshToken = oDT.Rows[0]["refresh_token"].ToString();

                    //Se verifica que la fecha de expiración sea menor a la actual
                    if (Convert.ToDateTime(oDT.Rows[0]["expires_in"]) <= DateTime.Now.AddHours(3))
                    {
                        m = new Meli(Convert.ToInt64(ConfigurationManager.AppSettings["APP_ID"]), ConfigurationManager.AppSettings["APP_Secret"].ToString()); //AccessToken, RefreshToken);

                        //Se renueva el Token
                        m.refreshToken(RefreshToken);

                        //Se actualiza el token en BD para nuevos ingresos
                        oDB.ActualizarToken(m.AccessToken, m.RefreshToken, 21600);

                        //Se actualizan variables para la ejecución actual
                        AccessToken = m.AccessToken;
                        RefreshToken = m.RefreshToken;
                    }
                    else //El token está vigente, se utiliza como está
                    {
                        m = new Meli(Convert.ToInt64(ConfigurationManager.AppSettings["APP_ID"]), ConfigurationManager.AppSettings["APP_Secret"].ToString(), AccessToken);
                    }

                    //FinalizarPublicaciones(m, AccessToken);
                    //this.Close();

                    //Borra los errores de sincronización anteriores
                    oDB.Error_Truncate();

                    //Las imagenes se sincronizan diariamente a las 2AM
                    if (DateTime.Now.ToString("HH", oCi) == "02")
                    {
                        SincronizarImagenes("SUIZA", m, AccessToken);
                        SincronizarImagenes("ARMERIA", m, AccessToken);
                    }

                    //Si son las 00:00hs se sincronizan las descripciones de las publicaciones
                    blnSincronizaDescripciones = (DateTime.Now.ToString("HH", oCi) == "00");

                    #region "Sincroniza OUTDOOR"

                    SincronizaSubidos("SUIZA", m, AccessToken, blnSincronizaDescripciones);

                    SincronizaNuevos("SUIZA", m, AccessToken);

                    #endregion

                    #region "Sincroniza ARMERIA"

                    SincronizaSubidos("ARMERIA", m, AccessToken, blnSincronizaDescripciones);

                    SincronizaNuevos("ARMERIA", m, AccessToken);

                    #endregion

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SincronizaSubidos(string pEmpresa, Meli pMeli, string pAccessToken, bool pSincronizaDescripciones)
        {
            DBManager oDB = new DBManager();
            DataTable oDTSubidos = new DataTable();
            DataTable oDTArticulos = new DataTable();
            DataTable oDTTalles = new DataTable();
            DataTable oDTStock = new DataTable();
            try
            {
                oDTSubidos = oDB.Subidos_GetAll(pEmpresa);
                oDTArticulos = oDB.Articulos_GetAll(pEmpresa);
                oDTTalles = oDB.Talles_GetAll(pEmpresa);
                oDTStock = oDB.Stock_GetAll(pEmpresa);

                foreach (DataRow oSubido in oDTSubidos.Rows)
                {
                    DataRow[] oRowArticulo = oDTArticulos.Select("IdArticulo=" + oSubido["IdArticulo"]);

                    #region Parametros Conexion
                    var p = new RestSharp.Parameter();
                    p.Name = "access_token";
                    p.Value = pAccessToken;

                    var ps = new List<RestSharp.Parameter>();
                    ps.Add(p);
                    #endregion

                    if (oRowArticulo.Length > 0)

                    {
                        if (pSincronizaDescripciones)
                        {
                            ActualizaDescripcion(oSubido["IdMercadoLibre"].ToString(), oRowArticulo[0]["Descripcion"].ToString(), pMeli, ps);
                        }
                        bool Cambio = false;
                        //Por cada Subido se verifica si el articulo varió en Precio o Stock
                        if (oSubido["Stock"].ToString() != oRowArticulo[0]["stockactual"].ToString())
                        {
                            Cambio = true;
                        }
                        if (Convert.ToDecimal(oSubido["Precio"]).ToString() != GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString(("##########.##").Replace(",", ".")))
                        {
                            Cambio = true;
                        }
                        if (Cambio)
                        {

                            #region TratamientoImagenes
                            string URLSite = "";
                            string URLSite2 = "";
                            string URLSite3 = "";
                            string Titulo = "";
                            object[] pPictures = new object[3];
                            if (pEmpresa == "SUIZA")
                            {
                                URLSite = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oRowArticulo[0]["IdArticulo"] + ".jpg";
                                if (!ExisteFoto(URLSite, 60000))
                                {
                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oSubido["IdArticulo"]), "FALTA FOTO");
                                    continue;
                                }
                                pPictures[0] = new { source = URLSite };
                                URLSite2 = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oRowArticulo[0]["IdArticulo"] + "_1.jpg";
                                if (!ExisteFoto(URLSite2, 60000))
                                {
                                    URLSite2 = "";
                                }
                                else
                                {
                                    pPictures[1] = new { source = URLSite2 };
                                }
                                URLSite3 = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oRowArticulo[0]["IdArticulo"] + "_2.jpg";
                                if (!ExisteFoto(URLSite3, 60000))
                                {
                                    URLSite3 = "";
                                }
                                else
                                {
                                    pPictures[2] = new { source = URLSite3 };
                                }
                            }
                            else
                            {
                                URLSite = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oRowArticulo[0]["IdArticulo"] + ".jpg";
                                if (!ExisteFoto(URLSite, 60000))
                                {
                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oSubido["IdArticulo"]), "FALTA FOTO");
                                    continue;
                                }
                                pPictures[0] = new { source = URLSite };
                                URLSite2 = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oRowArticulo[0]["IdArticulo"] + "_1.jpg";
                                if (!ExisteFoto(URLSite2, 60000))
                                {
                                    URLSite2 = "";
                                }
                                else
                                {
                                    pPictures[1] = new { source = URLSite2 };
                                }
                                URLSite3 = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oRowArticulo[0]["IdArticulo"] + "_2.jpg";
                                if (!ExisteFoto(URLSite3, 60000))
                                {
                                    URLSite3 = "";
                                }
                                else
                                {
                                    pPictures[2] = new { source = URLSite3 };
                                }
                            }
                            #endregion

                            //Verificar si tiene variantes
                            var variantes = pMeli.Get("/items/" + oSubido["IdMercadoLibre"].ToString() + "/variations/", ps);

                            if (variantes.Content.ToString() != "[]")
                            {
                                var x = JsonConvert.DeserializeObject(variantes.Content);
                                var variaciones = ((Newtonsoft.Json.Linq.JArray)x).Children();

                                string IdVariacion = "";
                                string IdColorP = "";
                                string IdTalleML = "";
                                string IdTalleMLDesc = "";
                                object[] oVariations = new object[variaciones.Count()];
                                Int32 CantidadTalles = 0;
                                //Se verifica la cantidad total de talles con stock, por si se agregaron talles nuevos
                                DataTable oDTTallesTotales = oDB.Stock_GetBy(Convert.ToInt64(oRowArticulo[0]["IdArticulo"]));
                                //Columna agregada en memoria para marcar si cada variacion ya fue actualizada en ML
                                oDTTallesTotales.Columns.Add(new DataColumn("VariacionActualizada", Type.GetType("System.Boolean")));

                                //Se recorren todas las variaciones existentes para el producto en ML
                                foreach (var item in variaciones)
                                {
                                    IdVariacion = item.SelectToken("id").ToString();
                                    var talle = item.SelectToken("attribute_combinations").Children();

                                    foreach (var t in talle)
                                    {
                                        if (t.SelectToken("name").ToString().Contains("Color"))
                                        {
                                            IdColorP = t.SelectToken("value_name").ToString();
                                        }
                                        if (t.SelectToken("name").ToString() == "Talle")
                                        {
                                            //if (string.IsNullOrEmpty(t.SelectToken("value_id").ToString()))
                                            //{
                                                //Para el caso de Subrubro = GORRO
                                                IdTalleMLDesc = t.SelectToken("value_name").ToString();
                                            //}
                                            //else
                                            //{
                                            //    IdTalleML = t.SelectToken("value_id").ToString();
                                            //}
                                        }
                                    }
                                    //Se busca el stock de la combinación
                                    //Buscar en Talles para ver si hay disponibilidad
                                    DataRow[] oRowTalle = null; //= oDTTalles.Select("IdPadre=" + oSubido["IdArticulo"] + " AND (IdTalleML='" + IdTalleML + "' OR talle like '%" + IdTalleMLDesc + "%')");
                                    if (!string.IsNullOrEmpty(IdTalleMLDesc))
                                    {
                                        oRowTalle = oDTTalles.Select("IdPadre=" + oSubido["IdArticulo"] + " AND (talle like '%" + "talle: " + IdTalleMLDesc +  "%')");
                                    }
                                    //if (!string.IsNullOrEmpty(IdTalleML))
                                    //{
                                    //    oRowTalle = oDTTalles.Select("IdPadre=" + oSubido["IdArticulo"] + " AND (IdTalleML='" + IdTalleML + "')");
                                    //}

                                    if (oRowTalle.Length > 0)
                                    {
                                        //Existe, verificar cantidad en stock y actualizar cantidad
                                        DataRow[] oRowStock = oDTStock.Select("IdArticulo=" + oRowTalle[0]["IdArticulo"]);
                                        if (oRowStock.Length > 0)
                                        {
                                            //Se suman las cantidades de todas las sucursales
                                            Int32 stockTotal = oRowStock.Sum(row => row.Field<Int32>("Stock"));
                                            Object oVaria = null;
                                            if ((!oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("GORRO") ||
                                                !oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("RIÑONERAS") ||
                                                !oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("MEDIAS") ||
                                                !oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("ACCESORIOS") ||
                                                !oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("GUANTES")) &&
                                                !oRowArticulo[0]["rubro"].ToString().ToUpper().Contains("INDUMENTARIA DE CACERIA"))
                                            {
                                                //Indumentaria o Calzado con Talle y Color
                                                oVaria = new
                                                {
                                                    id = IdVariacion,
                                                    //attribute_combinations = new object[] { new { name = "Talle", value_id = oRowTalle[0]["IdTalleML"].ToString() },
                                                    attribute_combinations = new object[] { new { name = "Talle", value_name = oRowTalle[0]["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", "U") },
                                                    new { name = "Color", value_name = String.IsNullOrEmpty( oRowTalle[0]["ColorDesc"].ToString()) ? IdColorP : oRowTalle[0]["ColorDesc"].ToString() }},
                                                    price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                                    available_quantity = stockTotal,
                                                    status = "active",
                                                    attributes = new object[] { new { id = "EAN", value_name = oRowTalle[0]["CodEAN"].ToString() } }
                                                };
                                                oDTTallesTotales.Select("Talle = '" + oRowTalle[0]["talle"].ToString() + "'")[0]["VariacionActualizada"] = true;
                                            }
                                            else
                                            {
                                                //Otras categorías más particulares
                                                if ((oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("GORRO") ||
                                                    oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("RIÑONERAS")) &&
                                                    !oRowArticulo[0]["rubro"].ToString().ToUpper().Contains("INDUMENTARIA DE CACERIA"))
                                                {
                                                    oVaria = new
                                                    {
                                                        id = IdVariacion,
                                                        attribute_combinations = new object[] { new { name = "Talle", value_name = oRowTalle[0]["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", " Unico") },
                                                                                new { name = "Color", value_name = String.IsNullOrEmpty( oRowTalle[0]["ColorDesc"].ToString()) ? IdColorP : oRowTalle[0]["ColorDesc"].ToString() }
                                                                            },
                                                        price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                                        available_quantity = stockTotal,
                                                        status = "active",
                                                        attributes = new object[] { new { id = "EAN", value_name = oRowTalle[0]["CodEAN"].ToString() } }
                                                    };
                                                }
                                                if (oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("MEDIAS") ||
                                                    oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("ACCESORIOS") ||
                                                    oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("GUANTES") ||
                                                    oRowArticulo[0]["rubro"].ToString().ToUpper().Contains("INDUMENTARIA DE CACERIA")
                                                    )
                                                {
                                                    oVaria = new
                                                    {
                                                        id = IdVariacion,
                                                        attribute_combinations = new object[] { new { name = "Talle", value_name = oRowTalle[0]["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", " Unico") } },
                                                        price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                                        available_quantity = stockTotal,
                                                        status = "active",
                                                        attributes = new object[] { new { id = "EAN", value_name = oRowTalle[0]["CodEAN"].ToString() } }
                                                    };
                                                }
                                                oDTTallesTotales.Select("Talle = '" + oRowTalle[0]["talle"].ToString() + "'")[0]["VariacionActualizada"] = true;
                                            }
                                            oVariations[CantidadTalles] = oVaria;
                                        }
                                        else
                                        {
                                            Object oVaria = null;
                                            oVaria = new
                                            {
                                                id = IdVariacion,
                                                price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                                available_quantity = 0
                                            };
                                            oVariations[CantidadTalles] = oVaria;
                                        }
                                    }
                                    else //No existe cantidad en stock 
                                    {
                                        Object oVaria = null;
                                        oVaria = new
                                        {
                                            id = IdVariacion,
                                            price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                            available_quantity = 0,
                                        };
                                        oVariations[CantidadTalles] = oVaria;
                                    }
                                    CantidadTalles += 1;
                                }

                                if (oDTTallesTotales.Select("VariacionActualizada is null").Length > 0)
                                {
                                    DataRow[] oVariacionesNuevas;
                                    oVariacionesNuevas = oDTTallesTotales.Select("VariacionActualizada is null");

                                    if (!oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("GORRO") ||
                                                !oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("RIÑONERAS") ||
                                                !oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("MEDIAS") ||
                                                !oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("ACCESORIOS") ||
                                                !oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("GUANTES"))
                                    {
                                        //Es rubro calzado o indumentaria que definen talle y color
                                        foreach (DataRow oVariacionNueva in oVariacionesNuevas)
                                        {
                                            //Tiene datos de Color y Talle completos
                                            //if (!string.IsNullOrEmpty(oVariacionNueva["IdTalleML"].ToString()) && !string.IsNullOrEmpty(oVariacionNueva["IdColorP"].ToString()))
                                            //{
                                                //Se agrega un indice para una nueva variación
                                                Array.Resize(ref oVariations, oVariations.Length + 1);
                                                Object oVaria = new
                                                {
                                                    attribute_combinations = new object[] { new { name = "Talle", value_name = oVariacionNueva["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", "U") },
                                                                                    //new { name = "Color Primario", value_id = oVariacionNueva["IdColorP"].ToString() }
                                                                                    new { name = "Color", value_name = string.IsNullOrEmpty( oVariacionNueva["ColorDesc"].ToString()) ? IdColorP : oVariacionNueva["ColorDesc"].ToString() }
                                                                                },
                                                    price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                                    available_quantity = oVariacionNueva["TotalStock"],
                                                    attributes = new object[] { new { id = "EAN", value_name = oVariacionNueva["CodEAN"].ToString() } },
                                                    picture_ids = new object[] { URLSite }
                                                };
                                                oVariations[oVariations.Length - 1] = oVaria;
                                            //}
                                            //else//Faltan IdTalleML o Color
                                            //{
                                            //    oDB.ErrorDatos_Insert(Convert.ToInt64(oVariacionNueva["IdArticulo"]), oRowArticulo[0]["Nombre"].ToString() + " - " + oVariacionNueva["Talle"].ToString() + " ==>> " + "AGREGAR TALLE y COLOR PRIMARIO Mercado Libre");
                                            //}
                                        }
                                    }
                                    else //Categorías particulas para Gorros, Riñoneras, accesorios, etc
                                    {
                                        //Se agrega un indice para una nueva variación
                                        Array.Resize(ref oVariations, oVariations.Length + 1);
                                        //Otras categorías más particulares
                                        if (oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("GORRO") ||
                                            oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("RIÑONERAS"))
                                        {
                                            oVariacionesNuevas = oDTTallesTotales.Select("VariacionActualizada is null");

                                            foreach (DataRow oVariacionNueva in oVariacionesNuevas)
                                            {
                                                if (!string.IsNullOrEmpty(oVariacionNueva["IdColorP"].ToString()))
                                                {
                                                    //Se agrega un indice para una nueva variación
                                                    Array.Resize(ref oVariations, oVariations.Length + 1);
                                                    Object oVaria = new
                                                    {
                                                        attribute_combinations = new object[] { new { name = "Talle", value_name = oVariacionNueva["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", " Unico") },
                                                                                new { name = "Color", value_id = String.IsNullOrEmpty( oVariacionNueva["ColorDesc"].ToString()) ? IdColorP : oVariacionNueva["ColorDesc"].ToString() }
                                                                            },
                                                        price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                                        available_quantity = oVariacionNueva["TotalStock"],
                                                        status = "active",
                                                        attributes = new object[] { new { id = "EAN", value_name = oVariacionNueva["CodEAN"].ToString() } }
                                                    };
                                                    oVariations[oVariations.Length - 1] = oVaria;
                                                }
                                                else
                                                {
                                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oVariacionNueva["IdArticulo"]), oRowArticulo[0]["Nombre"].ToString() + " - " + oVariacionNueva["Talle"].ToString() + " ==>> " + "AGREGAR COLOR PRIMARIO Mercado Libre");
                                                }
                                            }

                                        }
                                        if (oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("MEDIAS") ||
                                            oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("ACCESORIOS") ||
                                            oRowArticulo[0]["subrubro"].ToString().ToUpper().Contains("GUANTES")
                                            )
                                        {
                                            oVariacionesNuevas = oDTTallesTotales.Select("VariacionActualizada is null");

                                            foreach (DataRow oVariacionNueva in oVariacionesNuevas)
                                            {
                                                //Se agrega un indice para una nueva variación
                                                Array.Resize(ref oVariations, oVariations.Length + 1);
                                                Object oVaria = new
                                                {
                                                    id = IdVariacion,
                                                    attribute_combinations = new object[] { new { name = "Talle", value_name = oVariacionNueva["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", " Unico") } },
                                                    price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                                    available_quantity = oVariacionNueva["TotalStock"],
                                                    status = "active",
                                                    attributes = new object[] { new { id = "EAN", value_name = oVariacionNueva["CodEAN"].ToString() } }
                                                };
                                                oVariations[oVariations.Length - 1] = oVaria;
                                            }
                                        }

                                    }
                                }

                                if (oRowArticulo[0]["nombre"].ToString().Length > 60)
                                {
                                    Titulo = oRowArticulo[0]["nombre"].ToString().Substring(0, 60).Replace("'", "").Replace("''", "").Replace("&", "");
                                }
                                else
                                {
                                    Titulo = oRowArticulo[0]["nombre"].ToString().Replace("'", "").Replace("''", "").Replace("&", "");
                                }

                                Object oItem = new
                                {
                                    title = Titulo.Replace("&", " and ").Replace("/", " ").Replace("?", " "),
                                    //description = new { plain_text = oRowArticulo[0]["Descripcion"].ToString() },
                                    //price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                    variations = oVariations.Where(val => val != null).ToArray()
                                };

                                var rv = pMeli.Put("/items/" + oSubido["IdMercadoLibre"].ToString(), ps, oItem);

                                if (rv.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    //Se actualiza el registro en Subido
                                    oDB.Subidos_Update(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), oSubido["IdMercadoLibre"].ToString(), Convert.ToInt64(oRowArticulo[0]["stockactual"]), GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])));
                                }
                                else
                                {
                                    //Se genera el registro de Error en BD
                                    oDB.Error_Insert(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), rv.Content, rv.Request.Parameters.LastOrDefault().ToString());
                                }
                            }
                            else //No tiene variantes
                            {
                                if (oRowArticulo[0]["nombre"].ToString().Length > 60)
                                {
                                    Titulo = oRowArticulo[0]["nombre"].ToString().Substring(0, 60).Replace("'", "").Replace("''", "").Replace("&", "");
                                }
                                else
                                {
                                    Titulo = oRowArticulo[0]["nombre"].ToString().Replace("'", "").Replace("''", "").Replace("&", "");
                                }

                                var r = pMeli.Put("/items/" + oSubido["IdMercadoLibre"].ToString(), ps,
                                                                            new
                                                                            {
                                                                                title = Titulo.Replace("&", " and ").Replace("/", " ").Replace("?", " "),
                                                                                //description = new { plain_text = oRowArticulo[0]["Descripcion"].ToString() },
                                                                                price = GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])).ToString("##########.##").Replace(",", "."),
                                                                                available_quantity = oRowArticulo[0]["stockactual"].ToString(),
                                                                                attributes = new object[] { new { id = "EAN", value_name = oRowArticulo[0]["CodEAN"].ToString() } }
                                                                            });

                                if (r.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    //Se actualiza el registro en Subido
                                    oDB.Subidos_Update(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), oSubido["IdMercadoLibre"].ToString(), Convert.ToInt64(oRowArticulo[0]["stockactual"]), GetPrecioConIVA(Convert.ToDecimal(oRowArticulo[0]["precio"]), Convert.ToDecimal(oRowArticulo[0]["iva"])));
                                }
                                else
                                {
                                    //Se genera el registro de Error en BD
                                    oDB.Error_Insert(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), r.Content, r.Request.Parameters.LastOrDefault().ToString());
                                }
                            }

                        }
                    }
                    else //Ya no viene en xmlArticulosTalle, NO tiene stock
                    {
                        //Verificar si tiene variantes
                        var variantes = pMeli.Get("/items/" + oSubido["IdMercadoLibre"].ToString() + "/variations/", ps);

                        if (variantes.Content.ToString() != "[]")
                        {
                            var x = JsonConvert.DeserializeObject(variantes.Content);
                            var variaciones = ((Newtonsoft.Json.Linq.JArray)x).Children();

                            string IdVariacion = "";
                            object[] oVariations = new object[variaciones.Count()];
                            Int32 CantidadTalles = 0;
                            foreach (var item in variaciones)
                            {
                                IdVariacion = item.SelectToken("id").ToString();

                                Object oVaria = null;

                                oVaria = new
                                {
                                    id = IdVariacion,
                                    available_quantity = 0
                                };

                                oVariations[CantidadTalles] = oVaria;
                                CantidadTalles += 1;
                            }

                            Object oItem = new
                            {
                                variations = oVariations.Where(val => val != null).ToArray()
                            };

                            var rv = pMeli.Put("/items/" + oSubido["IdMercadoLibre"].ToString(), ps, oItem);

                            if (rv.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                //Se actualiza el registro en Subido
                                oDB.Subidos_Update(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), oSubido["IdMercadoLibre"].ToString(), 0, 0);
                            }
                            else
                            {
                                //Se genera el registro de Error en BD
                                oDB.Error_Insert(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), rv.Content, rv.Request.Parameters.LastOrDefault().ToString());
                            }

                        }
                        else //No tiene variantes
                        {
                            var r = pMeli.Put("/items/" + oSubido["IdMercadoLibre"].ToString(), ps,
                                                                        new
                                                                        {
                                                                            available_quantity = 0
                                                                        });

                            if (r.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                //Se actualiza el registro en Subido
                                oDB.Subidos_Update(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), oSubido["IdMercadoLibre"].ToString(), 0, 0);
                            }
                            else
                            {
                                //Se genera el registro de Error en BD
                                oDB.Error_Insert(Convert.ToInt64(oSubido["IdArticulo"]), oSubido["Empresa"].ToString(), r.Content, r.Request.Parameters.LastOrDefault().ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SincronizaNuevos(string pEmpresa, Meli pMeli, string pAccessToken)
        {
            DBManager oDB = new DBManager();
            DataTable oDTArticulos = new DataTable();
            DataTable oDTTalles = new DataTable();
            DataTable oDTStock = new DataTable();
            DataTable oDTMapeo = new DataTable();

            try
            {
                oDTArticulos = oDB.ArticulosNuevos_GetAll(pEmpresa);
                oDTTalles = oDB.Talles_GetAll(pEmpresa);
                oDTStock = oDB.Stock_GetAll(pEmpresa);
                oDTMapeo = oDB.MapeoCategoria_GetAll();

                foreach (DataRow oArticulo in oDTArticulos.Rows)
                {

                    #region Parametros Conexion
                    var p = new RestSharp.Parameter();
                    p.Name = "access_token";
                    p.Value = pAccessToken;

                    var ps = new List<RestSharp.Parameter>();
                    ps.Add(p);
                    #endregion

                    //Se verifica si va tener variaciones o no
                    if ((oArticulo["rubro"].ToString().Contains("INDUMENTARIA") ||
                        oArticulo["rubro"].ToString().Contains("CALZADO") ||
                        oArticulo["subrubro"].ToString().Contains("GORROS") ||
                        oArticulo["subrubro"].ToString().Contains("RIÑONERAS")) &&
                        !oArticulo["subrubro"].ToString().Contains("BUFANDA") &&
                        !oArticulo["subrubro"].ToString().Contains("MOCHILAS"))
                    {
                        string CategoriaML = "";

                        DataRow[] oRowMapeo = oDTMapeo.Select("Rubro='" + oArticulo["rubro"].ToString() + "' AND SubRubro='" + oArticulo["subrubro"].ToString() + "' AND Genero='" + oArticulo["Genero"].ToString() + "'");

                        if (oRowMapeo.Length > 0)
                        {
                            CategoriaML = oRowMapeo[0]["IdCategoriaML"].ToString();
                        }
                        else
                        {
                            var cate = pMeli.Get("/sites/MLA/category_predictor/predict?title='" + oArticulo["nombre"].ToString() + "'", ps);
                            if (cate.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                var x = JsonConvert.DeserializeObject(cate.Content);
                                CategoriaML = ((Newtonsoft.Json.Linq.JObject)x).SelectToken("id").ToString();
                            }
                        }

                        if (!string.IsNullOrEmpty(CategoriaML))
                        {

                            #region TratamientoImagenes
                            string URLSite = "";
                            string URLSite2 = "";
                            string URLSite3 = "";
                            string Titulo = "";
                            object[] pPictures = new object[3];
                            if (pEmpresa == "SUIZA")
                            {
                                URLSite = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + ".jpg";
                                if (!ExisteFoto(URLSite, 60000))
                                {
                                    //Si no tiene foto se ignora la publicación
                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), "SIN FOTO");
                                    continue;
                                }
                                pPictures[0] = new { source = URLSite };
                                URLSite2 = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + "_1.jpg";
                                if (!ExisteFoto(URLSite2, 60000))
                                {
                                    URLSite2 = "";
                                }
                                else
                                {
                                    pPictures[1] = new { source = URLSite2 };
                                }
                                URLSite3 = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + "_2.jpg";
                                if (!ExisteFoto(URLSite3, 60000))
                                {
                                    URLSite3 = "";
                                }
                                else
                                {
                                    pPictures[2] = new { source = URLSite3 };
                                }
                            }
                            else
                            {
                                URLSite = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + ".jpg";
                                if (!ExisteFoto(URLSite, 60000))
                                {
                                    //Si no tiene foto se ignora la publicación
                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), "SIN FOTO");
                                    continue;
                                }
                                pPictures[0] = new { source = URLSite };
                                URLSite2 = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + "_1.jpg";
                                if (!ExisteFoto(URLSite2, 60000))
                                {
                                    URLSite2 = "";
                                }
                                else
                                {
                                    pPictures[1] = new { source = URLSite2 };
                                }
                                URLSite3 = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + "_2.jpg";
                                if (!ExisteFoto(URLSite3, 60000))
                                {
                                    URLSite3 = "";
                                }
                                else
                                {
                                    pPictures[2] = new { source = URLSite3 };
                                }
                            }
                            #endregion

                            #region TratamientoTalles
                            DataRow[] oTalles = oDTTalles.Select("IdPadre=" + oArticulo["IdArticulo"].ToString());

                            object[] oVariations = new object[oTalles.Length];
                            Int32 CantidadTalles = 0;
                            foreach (DataRow oTalle in oTalles)
                            {
                                //Por cada talle se verifica que haya Stock
                                DataRow[] oStock = oDTStock.Select("IdArticulo=" + oTalle["IdArticulo"].ToString());
                                if (oStock.Length > 0)
                                {
                                    //Se suman las cantidades de todas las sucursales
                                    Int32 stockTotal = oStock.Sum(row => row.Field<Int32>("Stock"));
                                    Object oVaria = null;

                                    if (oArticulo["rubro"].ToString().Contains("INDUMENTARIA DE CACERIA"))
                                    {
                                        oVaria = new
                                        {
                                            attribute_combinations = new object[] { new { name = "Talle", value_name = oTalle["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", " Unico") } },
                                            price = GetPrecioConIVA(Convert.ToDecimal(oArticulo["precio"]), Convert.ToDecimal(oArticulo["iva"])).ToString("##########.##").Replace(",", "."),
                                            available_quantity = stockTotal,
                                            attributes = new object[] { new { id = "EAN", value_name = oTalle["CodEAN"].ToString() } },
                                            picture_ids = new object[] { URLSite }
                                        };
                                        oVariations[CantidadTalles] = oVaria;
                                    }
                                    else
                                    {
                                        switch (oArticulo["subrubro"].ToString())
                                        {
                                            case "GORRO":
                                            case "RIÑONERAS":
                                                //Se verifica que tenga los datos requeridos
                                                if (!String.IsNullOrEmpty(oTalle["IdTalleML"].ToString()) && !String.IsNullOrEmpty(oTalle["IdColorP"].ToString()))
                                                {
                                                    oVaria = new
                                                    {
                                                        attribute_combinations = new object[] { new { name = "Talle", value_name = oTalle["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", " Unico") },
                                                                                new { name = "Color Primario", value_id = oTalle["IdColorP"].ToString() } },
                                                        price = GetPrecioConIVA(Convert.ToDecimal(oArticulo["precio"]), Convert.ToDecimal(oArticulo["iva"])).ToString("##########.##").Replace(",", "."),
                                                        available_quantity = stockTotal,
                                                        attributes = new object[] { new { id = "EAN", value_name = oTalle["CodEAN"].ToString() } },
                                                        picture_ids = new object[] { URLSite }
                                                    };
                                                }
                                                else
                                                {
                                                    //Faltan datos de TalleML y ColorPrimario
                                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), oArticulo["Nombre"].ToString() + " - " + oTalle["Talle"].ToString() + " ==>> " + "AGREGAR COLOR PRIMARIO Mercado Libre");
                                                }
                                                break;
                                            case "MEDIAS":
                                            case "ACCESORIOS":
                                            case "GUANTES":
                                                oVaria = new
                                                {
                                                    attribute_combinations = new object[] { new { name = "Talle", value_name = oTalle["talle"].ToString().Replace("talle: ", "").Replace("talle: TALLE UNICO", " Unico") } },
                                                    price = GetPrecioConIVA(Convert.ToDecimal(oArticulo["precio"]), Convert.ToDecimal(oArticulo["iva"])).ToString("##########.##").Replace(",", "."),
                                                    available_quantity = stockTotal,
                                                    attributes = new object[] { new { id = "EAN", value_name = oTalle["CodEAN"].ToString() } },
                                                    picture_ids = new object[] { URLSite }
                                                };
                                                break;
                                            default:
                                                //Se verifica que tenga los datos requeridos
                                                if (!String.IsNullOrEmpty(oTalle["IdTalleML"].ToString()) && !String.IsNullOrEmpty(oTalle["IdColorP"].ToString()))
                                                {
                                                    oVaria = new
                                                    {
                                                        attribute_combinations = new object[] { new { name = "Talle", value_id = oTalle["IdTalleML"].ToString() },
                                                                                new { name = "Color Primario", value_id = oTalle["IdColorP"].ToString() }
                                                                            },
                                                        price = GetPrecioConIVA(Convert.ToDecimal(oArticulo["precio"]), Convert.ToDecimal(oArticulo["iva"])).ToString("##########.##").Replace(",", "."),
                                                        available_quantity = stockTotal,
                                                        attributes = new object[] { new { id = "EAN", value_name = oTalle["CodEAN"].ToString() } },
                                                        picture_ids = new object[] { URLSite }
                                                    };
                                                }
                                                else
                                                {
                                                    //Faltan datos de TalleML y ColorPrimario
                                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), oArticulo["Nombre"].ToString() + " - " + oTalle["Talle"].ToString() + " ==>> " + "AGREGAR TALLE y COLOR PRIMARIO Mercado Libre");
                                                }
                                                break;
                                        }
                                        oVariations[CantidadTalles] = oVaria;
                                    }
                                }
                                else
                                {
                                    oVariations[CantidadTalles] = null;
                                }
                                CantidadTalles += 1;
                            }
                            #endregion

                            if (oArticulo["nombre"].ToString().Length > 60)
                            {
                                Titulo = oArticulo["nombre"].ToString().Substring(0, 60).Replace("'", "").Replace("''", "").Replace("&", "");
                            }
                            else
                            {
                                Titulo = oArticulo["nombre"].ToString().Replace("'", "").Replace("''", "").Replace("&", "");
                            }

                            Object oItem = new
                            {
                                title = Titulo.Replace("&", " and ").Replace("/", " ").Replace("?", " "),
                                category_id = CategoriaML,
                                price = GetPrecioConIVA(Convert.ToDecimal(oArticulo["precio"]), Convert.ToDecimal(oArticulo["iva"])).ToString("##########.##").Replace(",", "."),
                                currency_id = "ARS",
                                available_quantity = oArticulo["stockactual"].ToString(),
                                buying_mode = "buy_it_now",
                                listing_type_id = "bronze",
                                condition = "new",
                                description = new { plain_text = oArticulo["Descripcion"].ToString() },
                                video_id = "",
                                warranty = "1 mes contra defecto de fábrica",
                                pictures = pPictures.Where(val => val != null).ToArray(),
                                variations = oVariations.Where(val => val != null).ToArray()
                            };

                            var r = pMeli.Post("/items", ps, oItem);

                            if (r.StatusCode == System.Net.HttpStatusCode.Created)
                            {
                                //Se inserta el registro en Subido
                                var id = JsonConvert.DeserializeObject(r.Content);
                                String idMercado = ((Newtonsoft.Json.Linq.JObject)id).SelectToken("id").ToString();

                                oDB.Nuevos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), pEmpresa, idMercado, Convert.ToInt64(oArticulo["stockactual"]), GetPrecioConIVA(Convert.ToDecimal(oArticulo["precio"]), Convert.ToDecimal(oArticulo["iva"])));
                            }
                            else
                            {
                                //Se genera el registro de Error en BD
                                oDB.Error_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), pEmpresa, r.Content, r.Request.Parameters.LastOrDefault().ToString());

                                //Error de variaciones duplicadas por mala carga de datos en Sinergia
                                if (r.Content.Contains("item.variations.duplicated"))
                                {
                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), "TALLES Mercado Libre DUPLICADOS");
                                }
                                if (r.Content.Contains("item.variations.attribute_combinations.invalid"))
                                {
                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), "TALLES Mercado Libre INVALIDOS");
                                }
                            }
                        }

                    }
                    else
                    {//Son productos sin variaciones
                        var cat = pMeli.Get("/sites/MLA/category_predictor/predict?title='" + oArticulo["nombre"].ToString() + "'", ps);

                        if (cat.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var a = JsonConvert.DeserializeObject(cat.Content);
                            String Categoria = ((Newtonsoft.Json.Linq.JObject)a).SelectToken("id").ToString();

                            #region TratamientoImagenes
                            string URLSite = "";
                            string URLSite2 = "";
                            string URLSite3 = "";
                            string Titulo = "";
                            object[] pPictures = new object[3];
                            if (pEmpresa == "SUIZA")
                            {
                                URLSite = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + ".jpg";
                                if (!ExisteFoto(URLSite, 60000))
                                {
                                    //Si no tiene foto se ignora la publicación
                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), "SIN FOTO");
                                    continue;
                                }
                                pPictures[0] = new { source = URLSite };
                                URLSite2 = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + "_1.jpg";
                                if (!ExisteFoto(URLSite2, 60000))
                                {
                                    URLSite2 = "";
                                }
                                else
                                {
                                    pPictures[1] = new { source = URLSite2 };
                                }
                                URLSite3 = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + "_2.jpg";
                                if (!ExisteFoto(URLSite3, 60000))
                                {
                                    URLSite3 = "";
                                }
                                else
                                {
                                    pPictures[2] = new { source = URLSite3 };
                                }
                            }
                            else
                            {
                                URLSite = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + ".jpg";
                                if (!ExisteFoto(URLSite, 60000))
                                {
                                    //Si no tiene foto se ignora la publicación
                                    oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), "SIN FOTO");
                                    continue;
                                }
                                pPictures[0] = new { source = URLSite };
                                URLSite2 = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + "_1.jpg";
                                if (!ExisteFoto(URLSite2, 60000))
                                {
                                    URLSite2 = "";
                                }
                                else
                                {
                                    pPictures[1] = new { source = URLSite2 };
                                }
                                URLSite3 = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + "_2.jpg";
                                if (!ExisteFoto(URLSite3, 60000))
                                {
                                    URLSite3 = "";
                                }
                                else
                                {
                                    pPictures[2] = new { source = URLSite3 };
                                }
                            }
                            #endregion

                            if (oArticulo["nombre"].ToString().Length > 60)
                            {
                                Titulo = oArticulo["nombre"].ToString().Substring(0, 60).Replace("'", "").Replace("''", "").Replace("&", "");
                            }
                            else
                            {

                                Titulo = oArticulo["nombre"].ToString().Replace("'", "").Replace("''", "").Replace("&", "");
                            }

                            Object oItem = new
                            {
                                title = Titulo.Replace("&", " and ").Replace("/", " ").Replace("?", " "),
                                category_id = Categoria,
                                price = GetPrecioConIVA(Convert.ToDecimal(oArticulo["precio"]), Convert.ToDecimal(oArticulo["iva"])).ToString("##########.##").Replace(",", "."),
                                currency_id = "ARS",
                                available_quantity = oArticulo["stockactual"].ToString(),
                                attributes = new object[] { new { id = "EAN", value_name = oArticulo["CodEAN"].ToString() } },
                                buying_mode = "buy_it_now",
                                listing_type_id = "bronze",
                                condition = "new",
                                description = new { plain_text = oArticulo["Descripcion"].ToString() },
                                video_id = "",
                                warranty = "1 mes contra defecto de fábrica",
                                pictures = pPictures.Where(val => val != null).ToArray()
                            };

                            var r = pMeli.Post("/items", ps, oItem);

                            if (r.StatusCode == System.Net.HttpStatusCode.Created)
                            {
                                //Se inserta el registro en Subido
                                var id = JsonConvert.DeserializeObject(r.Content);
                                String idMercado = ((Newtonsoft.Json.Linq.JObject)id).SelectToken("id").ToString();

                                oDB.Nuevos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), pEmpresa, idMercado, Convert.ToInt64(oArticulo["stockactual"]), GetPrecioConIVA(Convert.ToDecimal(oArticulo["precio"]), Convert.ToDecimal(oArticulo["iva"])));
                            }
                            else
                            {
                                //Se genera el registro de Error en BD
                                oDB.Error_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), pEmpresa, r.Content, r.Request.Parameters.LastOrDefault().ToString());
                            }
                        }
                        else
                        {
                            //Se genera el registro de Error en BD
                            oDB.Error_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), pEmpresa, cat.Content, cat.Request.Parameters.LastOrDefault().ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SincronizarImagenes(string pEmpresa, Meli pMeli, string pAccessToken)
        {
            DBManager oDB = new DBManager();
            DataTable oDTSubidos = new DataTable();

            try
            {
                oDTSubidos = oDB.Subidos_GetAll(pEmpresa);

                foreach (DataRow oArticulo in oDTSubidos.Rows)
                {
                    #region Parametros Conexion
                    var p = new RestSharp.Parameter();
                    p.Name = "access_token";
                    p.Value = pAccessToken;

                    var ps = new List<RestSharp.Parameter>();
                    ps.Add(p);
                    #endregion

                    var respon = pMeli.Get("/items/" + oArticulo["IdMercadoLibre"].ToString(), ps);// 
                    var x = JsonConvert.DeserializeObject(respon.Content);
                    var imagenes = ((Newtonsoft.Json.Linq.JObject)x).SelectToken("pictures").Children();

                    foreach (var img in imagenes)
                    {
                        var eliminar = pMeli.Delete("/pictures/" + img.SelectToken("id").ToString(), ps);
                        var eliminarrelacion = pMeli.Delete("/items/" + oArticulo["IdMercadoLibre"].ToString() + "/pictures/" + img.SelectToken("id").ToString(), ps);

                    }

                    #region TratamientoImagenes
                    string URLSite = "";
                    string URLSite2 = "";
                    string URLSite3 = "";
                    object[] pPictures = new object[3];
                    if (pEmpresa == "SUIZA")
                    {
                        URLSite = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + ".jpg";
                        if (!ExisteFoto(URLSite, 60000))
                        {
                            //Si no tiene foto se ignora la publicación
                            oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), "SIN FOTO");
                            continue;
                        }
                        pPictures[0] = new { source = URLSite };
                        URLSite2 = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + "_1.jpg";
                        if (!ExisteFoto(URLSite2, 60000))
                        {
                            URLSite2 = "";
                        }
                        else
                        {
                            pPictures[1] = new { source = URLSite2 };
                        }
                        URLSite3 = ConfigurationManager.AppSettings["URLImagenesOutdoor"].ToString() + oArticulo["IdArticulo"] + "_2.jpg";
                        if (!ExisteFoto(URLSite3, 60000))
                        {
                            URLSite3 = "";
                        }
                        else
                        {
                            pPictures[2] = new { source = URLSite3 };
                        }
                    }
                    else
                    {
                        URLSite = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + ".jpg";
                        if (!ExisteFoto(URLSite, 60000))
                        {
                            //Si no tiene foto se ignora la publicación
                            oDB.ErrorDatos_Insert(Convert.ToInt64(oArticulo["IdArticulo"]), "SIN FOTO");
                            continue;
                        }
                        pPictures[0] = new { source = URLSite };
                        URLSite2 = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + "_1.jpg";
                        if (!ExisteFoto(URLSite2, 60000))
                        {
                            URLSite2 = "";
                        }
                        else
                        {
                            pPictures[1] = new { source = URLSite2 };
                        }
                        URLSite3 = ConfigurationManager.AppSettings["URLImagenesArmeria"].ToString() + oArticulo["IdArticulo"] + "_2.jpg";
                        if (!ExisteFoto(URLSite3, 60000))
                        {
                            URLSite3 = "";
                        }
                        else
                        {
                            pPictures[2] = new { source = URLSite3 };
                        }
                    }
                    #endregion

                    //Verificar si tiene variantes
                    var variantes = pMeli.Get("/items/" + oArticulo["IdMercadoLibre"].ToString() + "/variations/", ps);

                    if (variantes.Content.ToString() != "[]")
                    {
                        var xi = JsonConvert.DeserializeObject(variantes.Content);
                        var variaciones = ((Newtonsoft.Json.Linq.JArray)xi).Children();

                        string IdVariacion = "";
                        object[] oVariations = new object[variaciones.Count()];
                        Int32 indice = 0;
                        foreach (var item in variaciones)
                        {
                            IdVariacion = item.SelectToken("id").ToString();

                            Object oVaria = null;
                            oVaria = new
                            {
                                id = IdVariacion,
                                picture_ids = new object[] { URLSite, URLSite2, URLSite3 }.Where(val => val.ToString() != "").ToArray()
                            };

                            oVariations[indice] = oVaria;
                            indice += 1;
                        }


                        Object oItem = new
                        {
                            pictures = pPictures.Where(val => val != null).ToArray(),
                            variations = oVariations.Where(val => val != null).ToArray()
                        };

                        var rv = pMeli.Put("/items/" + oArticulo["IdMercadoLibre"].ToString(), ps, oItem);

                    }
                    else
                    {
                        object itemimagenes = new
                        {
                            pictures = pPictures.Where(val => val != null).ToArray()
                        };

                        var nuevasimagenes = pMeli.Put("/items/" + oArticulo["IdMercadoLibre"].ToString() + "/", ps, itemimagenes);
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }


        }

        private void ActualizaDescripcion(string pIdMercadoLibre, string pDescripcion, Meli pMeli, List<RestSharp.Parameter> pPS)
        {
            try
            {
                var descripiones = pMeli.Get("/items/" + pIdMercadoLibre + "/descriptions/", pPS);

                if (descripiones.Content.ToString() != "[]")
                {
                    var x = JsonConvert.DeserializeObject(descripiones.Content);
                    var variaciones = ((Newtonsoft.Json.Linq.JArray)x).Children();

                    string IdDescripcion = "";
                    foreach (var item in variaciones)
                    {
                        IdDescripcion = item.SelectToken("id").ToString();
                        continue;
                    }
                    if (!string.IsNullOrEmpty(IdDescripcion))
                    {
                        var res = pMeli.Put("/items/" + pIdMercadoLibre + "/descriptions/" + IdDescripcion, pPS, new { plain_text = pDescripcion });

                        if (res.Content.ToString().Contains("Items with bids are not allow to modify the description"))
                        {
                            //Se genera una nueva descripción
                            var nueva = pMeli.Put("/items/" + pIdMercadoLibre + "/description", pPS, new { plain_text = pDescripcion });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void FinalizarPublicaciones(Meli pMeli, string pAccessToken)
        {
            DBManager oDB = new DBManager();
            DataTable oDTParaFinalizar = new DataTable();
            try
            {
                //for (int i = 0; i < 50; i++)
                //{
                //    #region Parametros Conexion
                //    var p = new RestSharp.Parameter();
                //    p.Name = "access_token";
                //    p.Value = pAccessToken;

                //    var ps = new List<RestSharp.Parameter>();
                //    ps.Add(p);

                //    var p2 = new RestSharp.Parameter();
                //    p2.Name = "limit";
                //    p2.Value = 50;
                //    ps.Add(p2);

                //    if (i > 0)
                //    {
                //        var p3 = new RestSharp.Parameter();
                //        p3.Name = "offset";
                //        p3.Value = 50 * i;
                //        ps.Add(p3);
                //    }

                //    #endregion

                //    //var Resultado = pMeli.Get("/sites/MLA/search?seller_id=314699137&", ps);//Homologacion
                //    var Resultado = pMeli.Get("/sites/MLA/search?seller_id=203107676&", ps);

                //    if (Resultado.StatusCode == System.Net.HttpStatusCode.OK)
                //    {
                //        var a = JsonConvert.DeserializeObject(Resultado.Content);
                //        var Publicaciones = ((Newtonsoft.Json.Linq.JObject)a).SelectToken("results").Children();
                //        using (StreamWriter writer = new StreamWriter("D:\\Publicaciones.txt", true, Encoding.UTF8))
                //        {
                //            foreach (var item in Publicaciones)
                //            {
                //                writer.WriteLine(item.SelectToken("id").ToString() + "|" + item.SelectToken("price").ToString().Replace(",", ".") + "|" + item.SelectToken("available_quantity").ToString());
                //            } 
                //        }
                //    }

                //}

                #region Parametros Conexion
                var p = new RestSharp.Parameter();
                p.Name = "access_token";
                p.Value = pAccessToken;

                var ps = new List<RestSharp.Parameter>();
                ps.Add(p);

                #endregion

                #region ObtenerYFinalizar
                oDTParaFinalizar = oDB.ParaFinalizar_GetAll();

                foreach (DataRow item in oDTParaFinalizar.Rows)
                {
                    var respuesta = pMeli.Put("/items/" + item["IdMercadoLibre"].ToString(), ps, new { status = "closed" });
                    if (respuesta.ResponseStatus == RestSharp.ResponseStatus.Completed)
                    {
                        string a = "";
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private decimal GetPrecioConIVA(decimal pPrecio, decimal pIVA)
        {
            decimal Retorno = 0;

            Retorno = (pPrecio + (pPrecio * pIVA) / 100) / Convert.ToDecimal(ConfigurationManager.AppSettings["DescuentoWeb"].ToString());

            return Retorno;
        }

        public static bool ExisteFoto(string url, int timeout)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                request = WebRequest.Create(url) as HttpWebRequest;

                // El timeout es en milisegundos
                request.Timeout = timeout;

                //Configurando el Request method HEAD, puede ser GET tambien.
                request.Method = "get";
                //Obteniendo la respuesta
                response = request.GetResponse() as HttpWebResponse;
                //Regresa TRUE si el codigo de esdado es == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                //Si ocurre una excepcion devuelve false
                return false;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
                response = null;
                request = null;
            }
        }

        private void EnviarMail(DateTime dtInicio, DateTime dtFin, bool EsError = false, string Error = "")
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient("mail.uariolabs.com", 25);

                smtpClient.Credentials = new System.Net.NetworkCredential("clientesuizaoutdoor@uariolabs.com", "SuizaOutdoor2018");
                smtpClient.UseDefaultCredentials = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                MailMessage mail = new MailMessage();

                //Setting From , To and CC
                mail.From = new MailAddress("clientesuizaoutdoor@uariolabs.com", "Sincronizador ML - Outdoor y Armeria");
                mail.To.Add(new MailAddress("matias@uariolabs.com"));
                mail.Subject = "Informe de Sincronización con Mercado Libre";
                mail.IsBodyHtml = true;

                if (EsError)
                {
                    mail.Body = "Se produjo un <b>ERROR</b> al sincronizar!<br/>";
                    mail.Body = mail.Body + "Error: " + Error + "<br/>";
                    mail.Body = mail.Body + "<b>Proceso Iniciado a:</b>   " + dtInicio.ToString("dd/MM/yyyy HH:mm:ss") + "<br/>";
                    mail.Body = mail.Body + "<b>Proceso Finalizado a:</b> " + dtFin.ToString("dd/MM/yyyy HH:mm:ss") + "<br/>";
                }
                else
                {
                    mail.Body = "Se sincronizaron <b>CORRECTAMENTE</b> los productos de SUIZA Outdoor";
                    mail.Body = mail.Body + "<b>Proceso Iniciado a:</b>   " + dtInicio.ToString("dd/MM/yyyy HH:mm:ss") + "<br/>";
                    mail.Body = mail.Body + "<b>Proceso Finalizado a:</b> " + dtFin.ToString("dd/MM/yyyy HH:mm:ss") + "<br/>";
                }

                smtpClient.Send(mail);
            }
            catch (Exception)
            {
                //throw;
            }
        }

    }
}